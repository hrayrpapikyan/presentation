function notify(data) {
        var st = data.status,
            notice = new PNotify({
                text: data.msg,
                type: data.status,
                delay: 3000,
                shadow: false,
                hide: true,
                buttons: {
                    closer: false,
                    sticker: true
                }
            });

}