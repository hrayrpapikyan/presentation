var Employee = function() {
    this.DataTableEmployees = $('#employees');
    this.editModal = $('#modal-edit');
    this.editForm = $('#edit-form');
    this.phonesTemplate = $('.phones-template');
    this.phonesContainer = $('#phones-container');
    this.addressesTemplate = $('.addresses-template');
    this.addressesContainer = $('#addresses-container');
    this.saveButton = $('.save-button');
    this.staticFields = [
        'id',
        'firstName',
        'lastName',
        'age',
        'city',
        'email',
        'country',
        'bankAccountNumber',
        'creditCardNumber'
    ];

    this.initDatatable = function() {
        this.DataTableEmployees.dataTable({
            bAutoWidth: false,
            bFilter: true,
            bInfo: true,
            bPaginate: true,
            bProcessing: true,
            iDisplayLength: 25,
            sPaginationType: "bootstrap",
            aaSorting: [[ 1, "asc" ]],
            aaData: aaData,
            aoColumns:[
                {
                    name: "checkbox",
                    sortable: false
                }, {
                    name: "first_name"
                }, {
                    name: "last_name"
                }, {
                    name: "age"
                },{
                    name: "city"
                }, {
                    name: "country"
                },{
                    name: "email"
                },{
                    name: "bank_number"
                },{
                    name: "cc"
                },{
                    name: "phones"
                },{
                    name: "addresses"
                }, {
                    name: "edit",
                    sortable: false,
                    searchable: false,
                    width: '1'
                }, {
                    name: "delete",
                    sortable: false,
                    searchable: false,
                    width: '1'
                }
            ]
        });
    };

    this.clearAllFormFields = function() {
        this.editForm.find('input').each(function() {
            $(this).val('');
        });
        this.addressesContainer.html('');
        this.phonesContainer.html('');
    };

    this.addPhone = function() {
        var value = '';
        if (arguments.length != 0)
            value = arguments[0];

        var $container =  this.phonesTemplate.clone().removeClass('hidden').removeClass('phones-template');
        $container.find('input').val(value);
        $container.appendTo(this.phonesContainer)
    };

    this.addAddress = function() {
        var value = '';
        if (arguments.length != 0)
            value = arguments[0];

        var $container =  this.addressesTemplate.clone().removeClass('hidden').removeClass('addresses-template');
        $container.find('input').val(value);
        $container.appendTo(this.addressesContainer)

    };

    this.openModalForCreate = function() {
        this.clearAllFormFields();
        this.addPhone();
        this.addAddress();
        this.editModal.modal('show');
    };

    this.collectData = function() {
        var data = {};
        $.each(this.staticFields, function (index, value) {
            data[value] = $('input[name="' + value + '"]').val();
        });
        data.phones = [];
        $('input[name="phones[]"]').each(function(){
            if ($(this).val() != '') {
                data.phones.push($(this).val());
            }
        });

        data.addresses = [];
        $('input[name="addresses[]"]').each(function(){
            if ($(this).val() != '') {
                data.addresses.push($(this).val());
            }
        });

        return data;
    };

    this.saveEmployee = function () {
        var self = this;
        $.ajax({
            url: '/employee/save',
            type: 'POST',
            dataType: "json",
            data: self.collectData(),
            error: function () {
                notify({
                    status: 'error',
                    msg: 'Unknown error'
                });
            },
            success: function (data) {

                notify(data);
                if (data.status == "success") {
                    self.editModal.modal('hide');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1500);
                }
            }
        });

    };

    this.deleteEmployee = function (ids) {
        var self = this;
        $.ajax({
            url: '/employee/delete',
            type: 'POST',
            dataType: "text",
            data: {
                ids: ids
            },
            error: function () {
                notify({
                    status: 'error',
                    msg: 'Unknown error'
                });
            },
            success: function (data) {
                data = JSON.parse(data);
                notify(data);
                if (data.status == "success") {
                    setTimeout(function(){
                        window.location.reload();
                    }, 1500);
                }
            }
        });
    };

    this.editEmployee = function (id) {
        var self = this;
        $.ajax({
            url: '/employee/edit',
            type: 'POST',
            dataType: "json",
            data: {
                id: id
            },
            error: function () {
                notify({
                    status: 'error',
                    msg: 'Unknown error'
                });
            },
            success: function (data) {

                if (data.status == "success") {
                    var phones = data.data.phones.split('</br>');
                    var addresses = data.data.addresses.split('</br>');

                    self.openModalForCreate();
                    self.editForm.find('input[name="id"]').val(data.data.id);

                    $.each(self.staticFields, function (index, value) {
                        self.editForm.find('input[name="' + value + '"]').val(data.data[value]);
                    });

                    self.editForm.find('input[name="phones[]"]').parents('.form-group.phone').remove();
                    $.each(phones, function( key, value ) {
                        self.addPhone(value);
                    });

                    self.editForm.find('input[name="addresses[]"]').parents('.form-group.address').remove();
                    $.each(addresses, function( key, value ) {
                        self.addAddress(value);
                    });
                }
            }
        });
    };


    this.startListen = function(){
        var self = this;
        $('.ui-pnotify.ui-pnotify-fade-normal.ui-pnotify-in.ui-pnotify-fade-in.ui-pnotify-move').css({'position':'absolute', "left":'0', "right": '0'});
        $('.delete_selected').attr('disabled', 'disabled');

        $('#add-employee').click(function (e) {
            e.preventDefault();
            self.openModalForCreate();
        });

        $('#employees').find('input[type="checkbox"]').on('click', function(event) {
            if( $('#employees').find('input[type="checkbox"]').is(':checked'))
                $('.delete_selected').removeAttr('disabled');
            else
                $('.delete_selected').attr('disabled', 'disabled');
        });

        $(document).on('click', '.add-phone', function(event) {
            event.preventDefault();
            self.addPhone();
        });

        $(document).on('click', '.delete-phone', function(event) {
            event.preventDefault();
            $(this).closest('.phone').remove();
        });

        $(document).on('click', '.add-address', function(event) {
            event.preventDefault();
            self.addAddress();
        });
        $(document).on('click', '.delete-address', function(event) {
            event.preventDefault();
            $(this).closest('.address').remove();
        });

        $(document).on('click', '.edit_record', function(event) {
            event.preventDefault();
            self.editEmployee($(this).attr('data-id'));
        });

        $(document).on('click', '.delete_record', function(event) {
            event.preventDefault();
            self.deleteEmployee([$(this).attr('data-id')]);
        });

        $(document).on('click', '.delete_selected', function(event) {
            event.preventDefault();
            var arIds = [];
            $.each($('#employees tbody tr td input[type="checkbox"]'), function( key, value ) {
                if($(this).is(':checked'))
                    arIds.push($(this).attr('data-id'));
            });
            self.deleteEmployee(arIds);
        });

        $(document).on('click', '.delete_all', function(event) {
            event.preventDefault();
            var arIds = [-1];
            self.deleteEmployee(arIds);
        });

        this.saveButton.click(function(e) {
            e.preventDefault();
            self.saveEmployee();
        })
    };
};

$(function(){
    var employee = new Employee();
    employee.initDatatable();
    employee.startListen();
});
