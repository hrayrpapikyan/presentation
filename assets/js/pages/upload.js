$(function(){

    var ul = $('#upload ul');

    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),
        dataType: 'json',
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
            $('#drop a').html('<img src="assets/img/loading.gif" width="50" height="50">');
            data.submit();
        },

        done: function ( e, data ) {
            $('#drop a').html('BROWSE');
            notify(data.result);
        },
        fail:function(e, data){
            $('#drop a').html('BROWSE');
            notify(data.result);
        }

    });

    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });


});