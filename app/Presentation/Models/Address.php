<?php
namespace Presentation\Models;
use Presentation\Core\Constants\Db;
use Presentation\Core\BaseModel;

class Address extends BaseModel
{
    public function __construct()
    {
        $this->setTable(Db::TABLE_ADDRESS);
        parent::__construct();
    }

    public function insertAddress(int $employeeId, array $addresses): void
    {
        $sql = "DELETE FROM " . $this->getTable() . " WHERE employee_id = ".$employeeId;
        $result = $this->getConnection()->query($sql);

        $stmt = $this->getConnection()->prepare("INSERT INTO " . $this->getTable() . " (employee_id, address)
    VALUES (:employee_id, :address)");


        foreach ($addresses as $address) {
            $stmt->bindParam(':employee_id', $employeeId);
            $stmt->bindParam(':address', $address);
            $stmt->execute();
        }


    }
}