<?php
namespace Presentation\Models;
use Presentation\Core\Constants\Db;
use Presentation\Core\BaseModel;

class Employee extends BaseModel
{

    const DELETE_ALL = -1;
    public function __construct()
    {
        $this->setTable(Db::TABLE_EMPLOYEE);
        parent::__construct();
    }

    public function insertEmployee(array $data): int
    {
        $stmt = $this->getConnection()->prepare("INSERT INTO " . $this->getTable() . " (firstName, lastName, age, city, email, country, bankAccountNumber, creditCardNumber)
    VALUES (:firstName, :lastName, :age, :city, :email, :country, :bankAccountNumber, :creditCardNumber)");
        $stmt->bindParam(':firstName', $data['firstName']);
        $stmt->bindParam(':lastName', $data['lastName']);
        $stmt->bindParam(':age', $data['age']);
        $stmt->bindParam(':city', $data['city']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':country', $data['country']);
        $stmt->bindParam(':bankAccountNumber', $data['bankAccountNumber']);
        $stmt->bindParam(':creditCardNumber', $data['creditCardNumber']);

        $stmt->execute();
        return $this->getConnection()->lastInsertId();
    }

    public function updateEmployee(array $data): void
    {
        $stmt = $this->getConnection()->prepare("UPDATE " . $this->getTable() . " SET firstName = :firstName, lastName = :lastName, age = :age, city = :city, email = :email, country = :country, bankAccountNumber = :bankAccountNumber, creditCardNumber = :creditCardNumber
    WHERE id = :id");
        $stmt->bindParam(':firstName', $data['firstName']);
        $stmt->bindParam(':lastName', $data['lastName']);
        $stmt->bindParam(':age', $data['age']);
        $stmt->bindParam(':city', $data['city']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':country', $data['country']);
        $stmt->bindParam(':bankAccountNumber', $data['bankAccountNumber']);
        $stmt->bindParam(':creditCardNumber', $data['creditCardNumber']);
        $stmt->bindParam(':id', $data['id']);

        $stmt->execute();
    }

    public function deleteEmployee(array $ids): void
    {
        $arIds = implode(',', $ids);

        $sql = "DELETE FROM " . $this->getTable();
        if($ids[0] != self::DELETE_ALL) {
            $sql .= " WHERE id IN ($arIds)";
        }

        $stmt = $this->getConnection()->prepare($sql);

        if ($arIds) {
            $stmt->bindParam(':id', $arIds);
        }

        $stmt->execute();
    }

    public function getAllEmployees(): array
    {
        $aaData = [];
        $sql = "
        SELECT
        " . $this->getTable() . ".*,
        GROUP_CONCAT(" . Db::TABLE_ADDRESS . ".address SEPARATOR '|') AS addresses,
        GROUP_CONCAT(" . Db::TABLE_PHONE . ".phone SEPARATOR '|') AS phones

        FROM " . $this->getTable() . "
        INNER JOIN " . Db::TABLE_PHONE . " ON employee.id = " . Db::TABLE_PHONE . ".employee_id
        INNER JOIN " . Db::TABLE_ADDRESS . " ON " . Db::TABLE_ADDRESS . ".employee_id = employee.id
        GROUP BY employee.id";
        $result = $this->getConnection()->query($sql, \PDO::FETCH_ASSOC);
        foreach ($result as $row) {

            $id = $row['id'];
            $aaData[] = [
                '<input type="checkbox" data-id="' . $id . '">',
                $row['firstName'],
                $row['lastName'],
                $row['age'],
                $row['city'],
                $row['country'],
                $row['email'],
                $row['bankAccountNumber'],
                $row['creditCardNumber'],
                implode('<br>', array_unique(explode('|',  $row['phones']))),
                implode('<br>', array_unique(explode('|',  $row['addresses']))),
                '<a href="#" class="btn btn-primary edit_record" data-id="' . $id . '">Edit</a>',
                '<a href="#" class="btn btn-danger delete_record" data-id="' . $id . '">Delete</a>'
            ];
        }

        return $aaData;

    }

    public function getEmployeeById(int $id): array
    {
        $aaData = [];
        $sql = "
        SELECT
        " . $this->getTable() . ".*,
        GROUP_CONCAT(" . Db::TABLE_ADDRESS . ".address SEPARATOR '</br>') AS addresses,
        GROUP_CONCAT(" . Db::TABLE_PHONE . ".phone SEPARATOR '</br>') AS phones
        FROM " . $this->getTable() . "
        INNER JOIN " . Db::TABLE_PHONE . " ON employee.id = " . Db::TABLE_PHONE . ".employee_id
        INNER JOIN " . Db::TABLE_ADDRESS . " ON " . Db::TABLE_ADDRESS . ".employee_id = employee.id
        WHERE employee.id = ". $id;
        $result = $this->getConnection()->query($sql);

        foreach ($result as $row) {
            $id = $row['id'];
            $aaData = [
                'id' => $id,
                'firstName' => $row['firstName'],
                'lastName' => $row['lastName'],
                'age' => $row['age'],
                'city' => $row['city'],
                'country' => $row['country'],
                'email' => $row['email'],
                'bankAccountNumber' => $row['bankAccountNumber'],
                'creditCardNumber' => $row['creditCardNumber'],
                'phones' => $row['phones'],
                'addresses' => $row['addresses'],
            ];
        }

        return $aaData;

    }


}