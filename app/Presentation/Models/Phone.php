<?php
namespace Presentation\Models;
use Presentation\Core\Constants\Db;
use Presentation\Core\BaseModel;

class Phone extends BaseModel
{
    public function __construct()
    {
        $this->setTable(Db::TABLE_PHONE);
        parent::__construct();
    }

    public function insertPhone(int $employeeId, array $phones): void
    {

        $sql = "DELETE FROM " . $this->getTable() . " WHERE employee_id = ".$employeeId;
        $result = $this->getConnection()->query($sql);

        $stmt = $this->getConnection()->prepare("INSERT INTO " . $this->getTable() . " (employee_id, phone)
    VALUES (:employee_id, :phone)");

        foreach ($phones as $phone) {
            $stmt->bindParam(':employee_id', $employeeId);
            $stmt->bindParam(':phone', $phone);
            $stmt->execute();
        }


    }
}