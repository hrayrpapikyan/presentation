<strong>
    Message:
</strong> <br>
<?php
/**
 * @var \Exception $exception
 */
?>
<p><?php echo $exception->getMessage();?></p>

<strong>
    Trace:
</strong> <br>
<p><?php echo $exception->getTraceAsString();?></p>