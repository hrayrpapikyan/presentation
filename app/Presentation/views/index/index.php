<?php
$this->addPageSpecificCss('upload.css')
     ->addPageSpecificJs('jquery.ui.widget.js')
     ->addPageSpecificJs('jquery.fileupload.js')
     ->addPageSpecificJs('upload.js');
?>
<div class="col-lg-12 text-center">
    <form id="upload" method="post" action="<?php echo $this->getBaseUrl() . 'index/upload'?>" enctype="multipart/form-data">
        <div id="drop">
            Drop Here

            <a>Browse</a>
            <input type="file" name="upl" multiple />
        </div>

        <ul>

        </ul>

    </form>
</div>