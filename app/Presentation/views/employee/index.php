<?php
$this->addPageSpecificCss('dataTables.bootstrap.min.css')
    ->addPageSpecificJs('jquery.datatables.min.js')
    ->addPageSpecificJs('DT_bootstrap.js')
    ->addPageSpecificJs('employee.js');
?>
<script>
    aaData = <?php echo json_encode($aaData)?>;
</script>
<div class="col-sm-12">
    <table id="employees" class="table table-striped table-bordered table-condensed table-hover">
        <thead>
        <tr>
            <th></th>
            <th> First Name </th>
            <th> Last Name </th>
            <th> Age </th>
            <th> City </th>
            <th> Country </th>
            <th> Email </th>
            <th> Bank Acc. number </th>
            <th> CC Number </th>
            <th> Phones </th>
            <th> Addresses </th>
            <th> &nbsp; </th>
            <th> &nbsp; </th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-3">
        <a href="#" class="btn btn-success" id="add-employee">Add Employee</a>
    </div>
    <div class="col-sm-3">
        <a href="#" class="btn btn-danger delete_selected">Delete Selected</a>
    </div>
    <div class="col-sm-3">
        <a href="#" class="btn btn-danger delete_all">Delete All</a>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<!--                <h4 class="modal-title">Add Employee</h4>-->
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="edit-form">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-md-3 control-label">First Name <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="firstName" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Last Name <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="lastName" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Age <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="age" type="text" class="form-control input-md">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">City <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="city" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Country <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="country" type="text" class="form-control input-md">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Email <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="email" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Bank Acc. Number <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="bankAccountNumber" type="text" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Credit Card Number <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-9">
                                <input name="creditCardNumber" type="text" class="form-control input-md">
                            </div>
                        </div>
                        <input name="id" type="hidden">
                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Phones <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-1">
                                <button class="btn btn-success add-phone"><i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>

                        <div id="phones-container">

                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Addresses <span class="text-danger glyphicon glyphicon-star"></span></label>
                            <div class="col-md-1">
                                <button class="btn btn-success add-address"><i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>

                        <div id="addresses-container">

                        </div>



                    </fieldset>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-button">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="form-group phone phones-template hidden">
    <div class="col-md-9">
        <input name="phones[]" type="text" class="form-control input-md">
    </div>
    <div class="col-md-1">
        <button class="btn btn-danger delete-phone"><i class="glyphicon glyphicon-minus"></i></button>
    </div>
</div>

<div class="form-group address addresses-template hidden">
    <div class="col-md-9">
        <input name="addresses[]" type="text" class="form-control input-md">
    </div>
    <div class="col-md-1">
        <button class="btn btn-danger delete-address"><i class="glyphicon glyphicon-minus"></i></button>
    </div>
</div>