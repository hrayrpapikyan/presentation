<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Employees</title>

    <link href="<?php echo $this->getAssetsPath();?>css/pnotify.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->getAssetsPath();?>css/bootstrap.min.css" rel="stylesheet">


    <?php foreach ($this->getPageSpecificCss() as $css) { ?>
        <link href="<?php echo $this->getAssetsPath();?>css/pages/<?php echo $css?>" rel="stylesheet">

    <?php } ?>

    <!-- Custom CSS -->
    <style>
        body {
            padding-top: 70px;
            /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo $this->getBaseUrl()?>">Import</a>
                </li>
                <li>
                    <a href="<?php echo $this->getBaseUrl()?>employee">List</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <?php echo $viewContent;?>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- jQuery Version 1.11.1 -->
<script src="<?php echo $this->getAssetsPath();?>js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $this->getAssetsPath();?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->getAssetsPath();?>js/pnotify.js"></script>
<script src="<?php echo $this->getAssetsPath();?>js/common.js"></script>

<?php foreach ($this->getPageSpecificJs() as $js) { ?>
    <script src="<?php echo $this->getAssetsPath();?>js/pages/<?php echo $js?>"></script>

<?php } ?>
</body>

</html>
