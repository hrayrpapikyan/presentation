<?php
namespace Presentation\Controllers;

use Presentation\Core\ViewModels\Html;
use Presentation\Core\ViewModels\Json;
use Presentation\Core\BaseController;
use Presentation\Models\Address;
use Presentation\Models\Employee as EmployeeModel;
use Presentation\Core\Validator;
use Presentation\Models\Phone;


class Employee extends BaseController
{
    public function index(): Html
    {
        $employeeModel = new EmployeeModel();
        $aaData = $employeeModel->getAllEmployees();
        return new Html([
            'aaData' => $aaData
        ]);
    }

    public function save(): Json
    {

        try {
            $employeeModel = new EmployeeModel();
            $phoneModel = new Phone();
            $addressModel = new Address();
            $validator = new Validator();
            $validator->add('firstName','First Name',  'Required|OnlyLetters');
            $validator->add('lastName','Last Name',  'Required|OnlyLetters');
            $validator->add('age','Age',  'Required|Numeric');
            $validator->add('city','City',  'Required|OnlyLetters');
            $validator->add('country','Country',  'Required|OnlyLetters');
            $validator->add('email','Email',  'Required|Email');
            $validator->add('bankAccountNumber','Bank Account Number',  'Required|Numeric');
            $validator->add('creditCardNumber','Credit Card Number',  'Required|Numeric');
            $validator->add('phones','Phones',  'Required');
            $validator->add('addresses','Addresses',  'Required');

            if (!empty($validator->getErrorMessages())) {
                return new Json( [
                    'status' => 'error',
                    'msg'    => implode('<br>', $validator->getErrorMessages())
                ]);
            }


            if (empty($_POST['id'])) {
                $msg = 'successfully Added';
                $employeeId = $employeeModel->insertEmployee($_POST);
                $phoneModel->insertPhone($employeeId, $_POST['phones']);
                $addressModel->insertAddress($employeeId, $_POST['addresses']);
            } else {
                $msg = 'successfully Updated';
                $employeeModel->updateEmployee($_POST);
                $phoneModel->insertPhone($_POST['id'], $_POST['phones']);
                $addressModel->insertAddress($_POST['id'], $_POST['addresses']);
            }


            return new Json( [
                'status' => 'success',
                'msg'    => $msg
            ]);
        } catch (\Exception $e){
            return new Json( [
                'status' => 'error',
                'msg'    => $e->getMessage()
            ]);
        }

    }

    public function delete(): Json
    {
        try {
            $employeeModel = new EmployeeModel();
            $validator = new Validator();
            $validator->add('ids','Employee',  'Required');

            if (!empty($validator->getErrorMessages())) {
                return new Json( [
                    'status' => 'error',
                    'msg'    => implode('<br>', $validator->getErrorMessages())
                ]);
            }

            $employeeModel->deleteEmployee($_POST['ids']);

            return new Json( [
                'status' => 'success',
                'msg'    => 'successfully Deleted'
            ]);
        } catch (\Exception $e){
            return new Json( [
                'status' => 'error',
                'msg'    => $e->getMessage()
            ]);
        }

    }

    public function edit(): Json
    {
        try {
            $employeeModel = new EmployeeModel();
            $validator = new Validator();
            $validator->add('id','Employee',  'Required');

            if (!empty($validator->getErrorMessages())) {
                return new Json( [
                    'status' => 'error',
                    'msg'    => implode('<br>', $validator->getErrorMessages())
                ]);
            }

            $data = $employeeModel->getEmployeeById($_POST['id']);

            return new Json([
                'status' => 'success',
                'data'    => $data
            ]);
        } catch (\Exception $e){
            return new Json( [
                'status' => 'error',
                'msg'    => $e->getMessage()
            ]);
        }

    }

}