<?php
namespace Presentation\Controllers;

use Presentation\Core\ViewModels\Html;
use Presentation\Core\ViewModels\Json;
use Presentation\Core\BaseController;
use Presentation\Services\JsonValidator;
use Presentation\Models\Employee;
use Presentation\Models\Phone;
use Presentation\Models\Address;

class Index extends BaseController
{
    public function index(): Html
    {
        return new Html();
    }

    public function upload(): Json
    {
        try {
            ini_set('max_execution_time', 0);
            if (!is_writable($this->getUploadsFolder())) {
                throw new \Exception('Uploads folder has no write permissions');
            }

            $allowed = ['json'];

            if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

                $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

                if(!in_array(strtolower($extension), $allowed)){
                   throw new \Exception('Uploaded file did not have allowed extension');
                }
                if(!move_uploaded_file($_FILES['upl']['tmp_name'], $this->getUploadsFolder() .'/' .$_FILES['upl']['name'])){
                    throw new \Exception('Could not move file to uploads directory');
                }

                $jsonValidator = new JsonValidator($this->getUploadsFolder() .'/' .$_FILES['upl']['name']);
                $convertedArray = $jsonValidator->validate();

                $employeesModel = new Employee();
                $phoneModel = new Phone();
                $addressModel = new Address();
                foreach ($convertedArray as $employee) {
                    $employee_id = $employeesModel->insertEmployee($employee);
                    $phoneModel->insertPhone($employee_id, $employee['phones']);
                    $addressModel->insertAddress($employee_id, $employee['addresses']);
                }
            } else {
                throw new \Exception('Unknown error on uploading file');
            }

            return new Json(
                [
                    'status' => 'success',
                    'msg'    => 'successfully imported'
                ]
            );
        } catch (\PDOException $e){
            return new Json(
                [
                    'status' => 'error',
                    'msg'    => 'Db insertion err: ' . $e->getMessage()
                ]
            );
        } catch (\Exception $e) {
            return new Json(
                [
                    'status' => 'error',
                    'msg' => $e->getMessage()
                ]
            );

        }
    }
}