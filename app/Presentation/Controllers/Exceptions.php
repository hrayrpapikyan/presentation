<?php
namespace Presentation\Controllers;

use Presentation\Core\ViewModels\Html;
use Presentation\Core\BaseController;


class Exceptions extends BaseController
{
    public function not_found(): Html
    {
        return new Html();
    }

}