<?php
namespace Presentation\Services;

class JsonValidator
{
    private $content;
    private $keys = [];
    public function __construct($jsonFilePath)
    {
        $this->content = file_get_contents($jsonFilePath);
        $this->keys = [
            'firstName',
            'lastName',
            'age',
            'city',
            'email',
            'country',
            'bankAccountNumber',
            'creditCardNumber',
            'phones',
            'addresses',
        ];
    }

    public function validate()
    {
        $array = json_decode($this->content, true);

        if (empty($array)) {
            throw new \Exception('Not valid json');
        }

        foreach ($array as $row) {
            foreach ($this->keys as $key) {
                if (!array_key_exists($key, $row)) {
                    throw new \Exception('Json contains row(s) without ' . $key);
                }
            }
        }

        return $array;
    }
}