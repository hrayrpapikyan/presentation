<?php
namespace Presentation\Core;

use Presentation\Core\Exceptions\RouteNotFoundException;

class Router
{
    /**
     * @var string
     */
    private $requestUri;

    /**
     * @var string
     */
    private $controller;

    /**
     * @var string
     */
    private $action;

    const DEFAULT_CONTROLLER = 'index';
    const DEFAULT_ACTION = 'index';

    public function __construct()
    {
        $this->setRequestUri($_SERVER['REQUEST_URI']);
    }


    public function dispatch(): void
    {
        if ($this->getRequestUri() == '' || $this->getRequestUri() == '/') {
            $this->setController(self::DEFAULT_CONTROLLER);
            $this->setAction(self::DEFAULT_ACTION);
        } else {
            $temp = explode('/', $this->getRequestUri());
            $this->setController($temp[0]);
            if (empty($temp[1]) || $temp[1] == '/') {
                $this->setAction(self::DEFAULT_ACTION);
            } else {
                $this->setAction($temp[1]);
            }
        }
        $this->validate();
    }

    private function validate(): void
    {
        if (!file_exists(__DIR__ . '/../Controllers/' . $this->getController() . '.php')) {
            throw new RouteNotFoundException();
        }

        if (!class_exists("Presentation\\Controllers\\" . $this->getController())) {
            throw new RouteNotFoundException();
        }

        if (!method_exists("Presentation\\Controllers\\" . $this->getController(), $this->getAction())) {
            throw new RouteNotFoundException('Method ' . "Presentation\\Controllers\\" . $this->getController() . '::' . $this->getAction() );
        }
    }


    public function getAction(): string
    {
        return ucfirst($this->action);
    }


    public function setAction(string $action): Router
    {
        $this->action = $action;
        return $this;
    }

    public function getController(): string
    {
        return ucfirst($this->controller);
    }


    public function setController(string $controller): Router
    {
        $this->controller = $controller;
        return $this;
    }

    public function getRequestUri(): string
    {
        return $this->requestUri;
    }

    public function setRequestUri(string $requestUri): Router
    {
        $this->requestUri = ltrim($requestUri, '/');
        return $this;
    }

}