<?php
namespace Presentation\Core\Exceptions;

class RouteNotFoundException extends \Exception{}