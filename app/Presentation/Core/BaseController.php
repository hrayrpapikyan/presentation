<?php
namespace Presentation\Core;

abstract class BaseController
{
    const DEFAULT_LAYOUT = 'main';

    /**
     * @var string
     */
    private $layout;

    /**
     * string
     */
    private $baseUrl;

    /**
     * string
     */
    private $uploadsFolder;


    public function __construct()
    {
        $this->setLayout(self::DEFAULT_LAYOUT);
        $this->setUploadsFolder( __DIR__ . '../../../../assets/uploads');
    }

    public function getLayout(): string
    {
        return $this->layout;
    }

    public function setLayout(string $layout): BaseController
    {
        $this->layout = $layout;
        return $this;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): BaseController
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }
    public function getUploadsFolder(): string
    {
        return $this->uploadsFolder;
    }

    public function setUploadsFolder(string $uploadsFolder): BaseController
    {
        $this->uploadsFolder = $uploadsFolder;
        return $this;
    }

}