<?php
namespace Presentation\Core;

use Presentation\Core\ViewModels\AbstractViewModel;
use Presentation\Core\ViewModels\Html;
use Presentation\Core\Exceptions\RouteNotFoundException;


class Application {
    /**
     * @var array
     */
    private static $config;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $baseUrl;

    CONST EXCEPTIONS_CONTROLLER = 'exceptions';
    CONST EXCEPTION_ACTION = 'exception';
    CONST EXCEPTION_NOT_FOUND_ACTION = 'not_found';

    public static function setConfig()
    {
        self::$config = require __DIR__ . "/../../config.php";
    }

    /**
     * @param bool|string $key
     * @return array|string
     * @throws \Exception
     */
    public static function getConfig($key = false)
    {
        if ($key === false) {
            return self::$config;
        }
        if (array_key_exists($key, self::$config)) {
            return self::$config[$key];
        }
        throw new \Exception('Configuration with given key does not exist');

    }
    public function __construct()
    {
        self::setConfig();
        $this
            ->setRouter(new Router())
            ->setBaseUrl($this->getConfig('base_url'));
    }

    public function start(): void
    {
        try {
            /**
             * @var \Presentation\Core\BaseController $controllerInstance
             */
            $this->router->dispatch();
            $controllerName = "Presentation\\Controllers\\"  .$this->router->getController();
            $controllerInstance = new $controllerName();
            $controllerInstance->setBaseUrl($this->getBaseUrl());
            $viewModel = call_user_func([$controllerInstance, $this->router->getAction()]);
            $this->checkAndRenderView($viewModel, $controllerInstance);
        } catch (RouteNotFoundException $e) {
            $this->redirect(self::EXCEPTIONS_CONTROLLER, self::EXCEPTION_NOT_FOUND_ACTION);
        }
        catch (\Exception $e) {
           $this->handleException($e);
        }

    }

    private function redirect(string $controller = Router::DEFAULT_CONTROLLER, string $action = Router::DEFAULT_ACTION): void
    {
        header('Location: ' . $this->getBaseUrl() . $controller . '/' . $action);
    }

    private function checkAndRenderView(AbstractViewModel $viewModel, BaseController $controllerInstance): void
    {
        if (!$viewModel instanceof AbstractViewModel) {
            throw new \Exception('Controller has not returned an instance of acceptable view model');
        }

        if ($viewModel instanceof Html) {
            $viewModel
                ->setLayout(strtolower($controllerInstance->getLayout()))
                ->setView(strtolower($this->router->getController()) . '/' . strtolower($this->router->getAction()))
                ->setBaseUrl($this->getBaseUrl());
        }

        $viewModel->render();
    }


    private function handleException(\Exception $e): void
    {
        $viewModel = new Html(['exception' => $e]);
        $viewModel->setLayout(BaseController::DEFAULT_LAYOUT)
            ->setView(self::EXCEPTIONS_CONTROLLER . '/' . self::EXCEPTION_ACTION)
            ->setBaseUrl($this->getBaseUrl());
        $viewModel->render();
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): Application
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    public function getRouter(): Router
    {
        return $this->router;
    }

    public function setRouter(Router $router): Application
    {
        $this->router = $router;
        return $this;
    }


}