<?php
namespace Presentation\Core\ViewModels;

class Json extends  AbstractViewModel
{

    public function render(): void
    {
        echo json_encode($this->getContent());
    }

}