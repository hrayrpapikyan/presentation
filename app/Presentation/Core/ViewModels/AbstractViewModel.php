<?php
namespace Presentation\Core\ViewModels;

abstract class AbstractViewModel
{
    /**
     * @var array
     */
    protected $content;

    public function __construct($content = [])
    {
        $this->setContent($content);
    }


    abstract public function render(): void;

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }

}