<?php
namespace Presentation\Core\ViewModels;

class Html extends AbstractViewModel
{
    /**
     * @var string
     */
    private $viewsDirectory;

    /**
     * @var string
     */
    private $layout;
    /**
     * @var string
     */
    private $view;

    /**
     * @var array
     */
    private $pageSpecificCss = [];

    /**
     * @var array
     */
    private $pageSpecificJs = [];


    /**
     * @var string $assetsPath
     */
    private $assetsPath;

    /**
     * @var string $baseUrl
     */
    private $baseUrl;

    public function __construct($content = [])
    {
        $this->setViewsDirectory(__DIR__ . '/../../views/');
        parent::__construct($content);
    }

    public function render(): void
    {
        $this->checkFilesExist();
        extract($this->getContent());
        $this->setAssetsPath($this->getBaseUrl() . 'assets/');
        ob_start();
        $viewContent = require $this->getView();
        $viewContent = ob_get_contents();
        ob_end_clean();

        require $this->getLayout();


    }

    private function checkFilesExist(): void
    {
        if (!file_exists($this->getLayout())) {
            throw new \Exception('Layout file does not exist: ' . $this->getLayout());
        }

        if (!file_exists($this->getView())) {
            throw new \Exception('View file does not exist: ' . $this->getView());
        }

    }


    public function getLayout(): string
    {
        return $this->layout;
    }

    public function setLayout(string $layout): Html
    {
        $this->layout = $this->getViewsDirectory() . 'layouts/' . $layout . '.php';
        return $this;
    }

    public function getView(): string
    {
        return $this->view;
    }

    public function setView(string $view): Html
    {
        $this->view = $this->getViewsDirectory() . $view . '.php';
        return $this;

    }

    public function getViewsDirectory(): string
    {
        return $this->viewsDirectory;
    }

    public function setViewsDirectory(string $viewsDirectory): Html
    {
        $this->viewsDirectory = $viewsDirectory;
        return $this;
    }

    public function getAssetsPath(): string
    {
        return $this->assetsPath;
    }


    public function setAssetsPath(string $assetsPath): Html
    {
        $this->assetsPath = $assetsPath;
        return $this;
    }

    private function addPageSpecificCss(string $css): Html
    {
        array_push($this->pageSpecificCss, $css);
        return $this;
    }

    private function addPageSpecificJs(string $js): Html
    {
        array_push($this->pageSpecificJs, $js);
        return $this;
    }

    public function getPageSpecificCss(): array
    {
        return $this->pageSpecificCss;
    }

    public function getPageSpecificJs(): array
    {
        return $this->pageSpecificJs;
    }


    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): Html
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }



}