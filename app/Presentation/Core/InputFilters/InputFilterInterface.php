<?php
namespace Presentation\Core\InputFilters;

interface InputFilterInterface
{
    public function validate(string $postField): void;
    public function getErrorMessage(): ?string;
}