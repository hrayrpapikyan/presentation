<?php
namespace Presentation\Core\InputFilters;

class Numeric implements InputFilterInterface
{
    CONST MESSAGE = 'shall contain only digits';
    /**
     * @var string|null
     */
    private $errorMessage = null;

    public function validate(string $postField): void
    {
        if (!empty($_POST[$postField])) {
            if (!is_numeric($_POST[$postField])) {
                $this->errorMessage = self::MESSAGE;
            }
        }
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}