<?php
namespace Presentation\Core\InputFilters;

class Required implements InputFilterInterface
{
    CONST MESSAGE = 'is required and can not be empty';
    /**
     * @var string|null
     */
    private $errorMessage = null;

    public function validate(string $postField): void
    {
        if (empty($_POST[$postField])) {
            $this->errorMessage = self::MESSAGE;
        }
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}