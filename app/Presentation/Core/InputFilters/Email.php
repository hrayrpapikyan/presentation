<?php
namespace Presentation\Core\InputFilters;

class Email implements InputFilterInterface
{
    CONST MESSAGE = 'is not a valid email';
    /**
     * @var string|null
     */
    private $errorMessage = null;


    public function validate(string $postField): void
    {
        if (!empty($_POST[$postField])) {
            if (!filter_var($_POST[$postField], FILTER_VALIDATE_EMAIL) === true) {
                $this->errorMessage = self::MESSAGE;
            }
        }
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}