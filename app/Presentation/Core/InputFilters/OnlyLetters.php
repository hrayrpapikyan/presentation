<?php
namespace Presentation\Core\InputFilters;

class OnlyLetters implements InputFilterInterface
{
    CONST MESSAGE = 'shall contain only letters';
    /**
     * @var string|null
     */
    private $errorMessage = null;

    public function validate(string $postField): void
    {
        if (!empty($_POST[$postField])) {
            if (ctype_alpha(str_replace(' ', '', $_POST[$postField])) === false) {
                $this->errorMessage = self::MESSAGE;
            }
        }
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}