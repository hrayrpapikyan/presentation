<?php
namespace Presentation\Core;

class DbConnection
{
    /**
     * @var \PDO
     */
    private static $connection = null;
    private function __construct(){}
    private function __clone(){}

    public static function getConnection(): \PDO
    {
        if (self::$connection === null)
        {
            $dbConfig = Application::getConfig('database');
            self::$connection = new \PDO("mysql:host=" . $dbConfig['servername'] . ";dbname=" . $dbConfig['dbname'], $dbConfig['username'], $dbConfig['password']);
            self::$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        return self::$connection;
    }
}