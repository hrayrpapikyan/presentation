<?php
namespace Presentation\Core;

class Validator
{
    /**
     * @var array
     */
    private $errorMessages = [];


    public function add(string $postField, string $presentationName, string $inputFilters)
    {
        $inputFilterArray = explode('|', $inputFilters);
        foreach ($inputFilterArray as $inputFilterName) {
            $inputFilterClass = 'Presentation\\Core\\InputFilters\\' . $inputFilterName;
            if (!class_exists($inputFilterClass)) {
                throw new \Exception('Requested Input Filter could not be found');
            }

            /**
             * @var \Presentation\Core\InputFilters\InputFilterInterface $inputFilterInstance
             */
            $inputFilterInstance = new $inputFilterClass();
            $inputFilterInstance->validate($postField);
            if ($inputFilterInstance->getErrorMessage() !== null) {
                array_push($this->errorMessages, $presentationName . ' ' . $inputFilterInstance->getErrorMessage());
            }
        }
    }

    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }


}