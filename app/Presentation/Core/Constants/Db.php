<?php
namespace Presentation\Core\Constants;

class Db
{
    const TABLE_EMPLOYEE = 'employee';
    const TABLE_PHONE = 'phone';
    const TABLE_ADDRESS = 'address';
}