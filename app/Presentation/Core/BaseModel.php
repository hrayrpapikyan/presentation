<?php
namespace Presentation\Core;

abstract class BaseModel
{
    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * @var string
     */
    protected $table;

    public function __construct()
    {
        $this->connection = DbConnection::getConnection();
    }

    public function getTable(): string
    {
        return $this->table;
    }

    public function setTable(string $table): BaseModel
    {
        $this->table = $table;
        return $this;
    }


    public function getConnection(): \PDO
    {
        return $this->connection;
    }

}