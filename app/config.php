<?php
return [
    'database' => [
        'servername' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'presentation',
    ],
    'base_url' => 'http://presentation.local/',
];