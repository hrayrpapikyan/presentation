Installation:

1. Install xampp and create local server for the application.

2. Install composer

3. open terminal and cd to application directory (where index.php is located)

4. type 'composer dumpautoload -o' to generate autoload related files.

5. open http://localhost/phpmyadmin, create a daabase and
import other/presentation.sql

6. open app/config.php and change configs corresponding to your database server credentials and base_url

7. restart xampp

8. have fun