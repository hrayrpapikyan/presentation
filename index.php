<?php
use Presentation\Core\Application;
require_once __DIR__ . "/vendor/autoload.php";
$application = new Application();
$application->start();
